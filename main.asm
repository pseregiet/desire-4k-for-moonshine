// 4K for moonshine / Desire
// coded by Golara in 2019

.var muzak = LoadSid("SSSS.sid")

.const techzp = $b0
.label regazp = $02
.label regxzp = $03
.label regyzp = $04
.label scrollzp = $05//$06
.label fppshaddowzp = $10
.label screenptr = $4800
.label charsetptr= $4800
.label scrollscreenptr = $4800 + 40*2
.label fppscreen = $5800
.label fppcharset = $5000

.const unstable_line_irq = 39
.const xsinezero = 122
.const fpplines = 128
.const main_screen_color = $0c
.const bottom_screen_color = $00
.const rasterbar1 = List().add($09,$02,$08,$0a,$0f,$07,$01,$07,$0f,$0a,$08,$02,$09)
.const rasterbar2 = List().add($06,$04,$0e,$05,$03,$0d,$01,$0d,$03,$05,$0e,$04,$06)


:BasicUpstart2(entry)
* = * "Entry code"
entry:
        sei
        cld
//uncoment this if you are not using a packer that will 
//set $01 after depack is complete
//      lda #$35
//      sta $01

        ldx #$ff
        txs
        // x = ff
        stx $dc02
        stx $d01c

        inx
        // x = 00
        stx $d01a
        stx $dc0e
        stx $dc0f
        stx $dd0e
        stx $dd0f
        stx $7fff
        stx bgline + bgstep*63
        stx $d020

// zero out memory $2400 - $58ff
{
        txa
!:      sta m:$2400,x
        inx
        bne !-
        ldy m+1
        iny
        sty m+1
        cpy #>$5900
        bne !-
}
        // x = 7f
        ldx #$7f
        stx $dc0d
        stx $dd0d
        stx $dc00
        ldx $dc0d
        ldx $dd0d

        lda #>scrolltext
        sta scroll+1
        lda #<scrolltext
        sta scroll
// format of sprites for FPP effect has a lot of zeros between the graphics data
// for smaller file size the graphics are all together and expanded to a format
// for use with FPP routine.
        :copy_fpp_sprites()

// to save on file size there's as few sinus tables as possible. To add more variety
// these tables are stored again in the memory with some modifications.
// There's also only half of a sinus (128 bytes) table. The other half is mirrored.
        lda #$6
        sta $06
!:
        jsr expand_tables
        jsr copy_tables
        dec $06
        bne !-
        jsr copy_tables
        :lsr_tables()
// generate speedcode for FPP routine. The generated code will be equalivant to this loop
/*
        .for (var i = 0; i < fpplines; i++)
        {
            .if (mod(i,2) ==0) { sta $d011 }                    ;1 does FLD (avoids badlines, moves screen down for scroller)
            else { sta $d021 }                                  ;2 changes background color (for rasterbars)

            stx $d017                                           ;3 set all sprites to double Y = 1
            lda #$00                                            ;4 this value changes at runtime, it's a number of a line of the logo
            sta fppscreen + $3f8 + 2
            sta fppscreen + $3f8 + 3
            sta fppscreen + $3f8 + 4
            sta fppscreen + $3f8 + 5
            sta fppscreen + $3f8 + 6
            sty $d017                                           ;5 set all sprites to double Y = 0 (causes VIC to draw the same line again, stretching)
            sty $d016                                           ;6 open sideborder
            sta fppscreen + $3f8 + 7
            stx $d016                                           ;7 reset border to normal for next line

            .if (mod(i,2) == 1) { lda #$18 + mod(i+3, 8) }      ;8 load value for FLD for next line
            else { lda #$00 }                                   ;9 load background color for next line
        }
*/
        lda #fpplines/2
        sta $06
        lda #<fpp_speedcode
        sta $04
        lda #>fpp_speedcode
        sta $05
!:
        jsr fppline1
        jsr fppline2
        dec $06
        bne !-
        jsr insert_rts
// generate speedcode clearing all the FPP sprite pointers (look at routine above, line 4)
        jsr make_clearfpp
// generate speedcode that copies the FPP sprite pointers (value at line 4) from zero page to the speedcode routine
.label copyshadowfpp = $b390
        lda #<copyshadowfpp
        sta $04
        lda #>copyshadowfpp
        sta $05
        lda #fpplines
        sta $06
!:
        jsr make_copy_fppshadowzp
        dec $06
        bne !-
        jsr insert_rts
// generate speedcode that selects colors for the raster bars
        lda #<scrollraster_speedcode
        sta $04
        lda #>scrollraster_speedcode
        sta $05
        lda #fpplines/2
        sta $06
!:
        jsr make_scrollraster_speedcode
        dec $06
        bne !-
// setup first irq
        lda #<unstable_irq
        sta $fffe
        lda #>unstable_irq
        sta $ffff
        lda #unstable_line_irq
        sta $d012
        lda #$1b
        sta $d011

        lda #00
        jsr muzak.init
// make the screen fill with black characters for a "nice" but tiny way of clearing the garbage
// left after the depacker.
        jsr intro
// copy colors for the bottom scroller
        ldx #39
!:
        .for (var i = 0; i < 10; i++)
        {
            .if (i == 0 || i > 8)
            {
                lda #$00
            }
            else
            {
                lda #rasterbar1.get(i+3)
            }
            sta $d800 + i*40, x
        }
        dex
        bpl !-
// change vic bank to $4000-$7fff
        // x = 02
        ldx #$02
        stx $dd00
        
        lda #1
        sta $d019
        sta $d01a
        cli
// unstable loop just to make sure my irq is stable.
!:
        inx
        bne !-
        jmp !-

waster_2:
        jsr waster_1
waster_1:
        jsr waster_jsr
waster_jsr:
        rts

* = * "Speedcode generator"
.import source "fppspeedcode.asm"


intro:
{
loop1:
        lda #$a0
        ldx #39
        //ldy #39
loop2:
        sta dst1:$400,x
        sta dst2:$d800,x
        dex
        bpl loop2

        clc
        lda dst1
        adc #40
        sta dst1
        sta dst2
        bcc !+
        inc dst1 + 1
        inc dst2 + 1
!:

        lda #255
!:      cmp $d012
        bne !-
        lda dst1+1
        cmp #>$800
        bne loop1
        

        rts
}

copy_tables:
{
        ldx #$00
        ldy #127
!:
        lda src:tabx1,y
        sta dst:tabx1+128,x
        inx
        dey
        bpl !-
        inc src+1
        inc dst+1
        rts
}
add16:  //preload <a value >x
{
        sty $07
        ldy tab:#$00
        clc
        adc adctab, y
        bcc !+
        inx
!:
        ldy $07
        rts
adctab:
.fill 6, 48*i
}

expand_tables:
{
        ldy #127
oneloop:
        lax orgtabhi,y
        lda orgtablo,y
        jsr add16
        cpx #$ff
        bne !+
        clc
        adc #$f8
!:
        sta dst:tabx1,y
        cpx #$00
        beq !+
        lda fppmove9thtab,y
        ora orer:#$04
        sta fppmove9thtab,y
!:

        dey
        bpl oneloop
        inc dst+1
        inc add16.tab
        asl orer
        rts
}
.macro lsr_tables()
{
        ldx #$00
!:
        lda rastersin,x
        lsr
        lsr
        lsr
        sta rastersin_lsr,x
        inx
        bne !-
}

// could use more palettes...
.const logocolortable = List().add(
    List().add($4,$a,$6),
    List().add($3,$6,$e)
)

colorfade:
{
        ldx step:#$00
        bmi wentblack
        cpx #$8
        beq end
        
        lda p1:tab04,x
        sta col1
        lda p2:tab0a,x
        sta col2
        lda p3:tab06,x
        sta col3
        lax step
        axs dir:#-1
        stx step
end:
        rts
wentblack:
        ldx palleteidx:#$01
        lda tab1lo,x
        sta p1
        lda tab2lo,x
        sta p2
        lda tab3lo,x
        sta p3
        inx
        cpx #logocolortable.size()
        bne !+
        ldx #$00
!:
        stx palleteidx

        ldx #-1
        stx dir
        inx
        stx step
        change_fpp_effect()
        rts

tab1lo:
.byte <tab04
.byte <tab03
tab2lo:
.byte <tab0a
.byte <tab06
tab3lo:
.byte <tab06
.byte <tab0e
}


.macro change_fpp_effect()
{
        ldx effectid:#$01
        lda fppcallslo,x
        sta fppcallptr+0
        lda fppcallshi,x
        sta fppcallptr+1
        lda fppgoidx
        sbc scroll
        sta fppgoidx

        inx
        cpx #$05
        bne !+
        ldx #$00
!:      stx effectid
}

fppcallslo:
.byte <fppfld1
.byte <fppfld2
.byte <fppgo
.byte <fppskip
.byte <fpp_stretch
fppcallshi:
.byte >fppfld1
.byte >fppfld2
.byte >fppgo
.byte >fppskip
.byte >fpp_stretch

// this irq just enables irqs again and sets up irq for a second line
// second irq will hit on a nop which means we have jitter of only 0 or 1 cycles
// because nop is a 2 cycles opcode... 
* = * "fpp irq"
unstable_irq:
        sta regazp
        stx regxzp
        sty regyzp

        lda #<unstable2_irq
        sta $fffe
        lda #>unstable2_irq
        sta $ffff
        inc $d012

        tsx
        inc $d019
        cli
        .for (var i = 0; i < 20; i++) { nop }
// no we need to get rid of the 1 cycle jitter. Classic 2 irq stable raster routine.
// while waiting for an end of a rasterline I setup some sprite registers for FPP.
unstable2_irq:
        txs
        lda #$fc
        sta $d015
        ldx #unstable_line_irq + 9
        stx $d001 + 2*2
        dex
        stx $d001 + 3*2
        dex
        stx $d001 + 4*2
        dex
        stx $d001 + 5*2
        dex
        stx $d001 + 6*2
        dex
        stx $d001 + 7*2

        nop        
        nop        
        lda #unstable_line_irq+1
        cmp $d012
        beq stable
stable:
        
        
        pha
        pla
        pha
        pla
        pha
        pla
        bit $ea

        inc $ff00
        lda #$ff
        sta $d01d
        lda #$18
        sta $d011

// this code is modified at runtime to move the logo left and right 
.const fppposx = * + 1
.const fppposstep = 5
        .for (var i = 0; i < 6; i++)
        {
                lda #00 + (i*48)
                sta $d000 + (2 + i)*2
        }
// start with blank sprites
        ldx #$00
        .for (var i = 0; i < 6; i++)
        {
                stx fppscreen + $3f8+2+i
        }
        

        lda #unstable_line_irq+10
!:      cmp $d012
        bne !-
        clc
        jsr waster_1

        lda col1:#$00//#$04
        sta $d025
        lda col2:#$00//#$0a
        sta $d026
        lda col3:#$00//#$06
        sta $d029
        sta $d02a
        sta $d02b
        sta $d02c
        sta $d02d
        sta $d02e

        nop
        nop
     
        lda #((fppscreen & $3fff) >> 6) | ((fppcharset&$3fff)/$800)*2
        sta $d018
        lda d10th:#00
        sta $d010
        lda #0 | $18
        ldx #00
        ldy #$ff
// start FPP routine (128 lines)
        jsr fpp_speedcode
// after FPP change the screen to scroller

        stx $d017
        lda #((screenptr & $3fff) >> 6) | ((charsetptr&$3fff)/$800)*2
        sta $d018
        lda #$18
        sta $d011
        ldx #0
        .for (var i = 0; i < 6; i++)
        {
                stx fppscreen + $3f8+2+i
        }

        lda scrollspeed:#$07
        sta $d016

        jsr muzak.play
        jsr clearfpp

        lda skipcolorfade:#$00
        and #1
        bne !+
        jsr colorfade
!:      inc skipcolorfade


        ldx timer1:#$00
        inx
        bne !+
        lda colorfade.dir
        eor #$fe
        sta colorfade.dir
        dec colorfade.step
!:
        stx timer1

        jsr fppcallptr:fppfld1
        jsr copyshadowfpp
        jsr movefpplogo
        jsr scrollraster

        lda scrollspeed
        sec
        sbc #6
        sta scrollspeed
        bpl !+
        clc
        adc #7
        sta scrollspeed
        jsr scroll_8x8
!:        

        lda #<unstable_irq
        sta $fffe
        lda #>unstable_irq
        sta $ffff
        lda #unstable_line_irq
        sta $d012
        inc $d019
        lda regazp
        ldx regxzp
        ldy regyzp
        rti
* = * "FPP ROUTINES 1"
// Routines for different FPP moves...
fpp_stretch:
{
        ldx fppgoidx
        stx $d2
        lda #$00
        sta $d0
l1:
        ldx $d2
        ldy rastersin_lsr, x
        ldx $d0
l2:
        sta fppshaddowzp, x
        inx
        dey
        bpl l2
        stx $d0
        inc $d2
        clc
        adc #1
        cmp #32
        beq end
        cpx #fpplines
        bne l1
end:
        inc fppgoidx
        rts
}
//====================================
fppgo:
        ldy fppgoidx:#00
        .for (var i = 0; i < 64; i++)
        {
            lax fppgotab, y
            lda #i/2
            sta fppshaddowzp, x
            dey
        }
        lax fppgoidx
        axs #$3
        stx fppgoidx
        rts
//====================================
fppfld:
        lsr
        tay
        lda #$00
        tax
!:
        sta fppshaddowzp,y
        iny
        sta fppshaddowzp,y
        iny
        inx
        txa
        cmp #32
        bne !-
        rts
//====================================
fppfld2:
        lda fppgoidx
        adc #77
        tay
        lax fppgotab, y
        jsr fppfld
        lax fppgoidx
        axs #-6
        stx fppgoidx
fppfld1:
        ldy fppgoidx
        lax fppgotab, y
        jsr fppfld
        lax fppgoidx
        axs #2
        stx fppgoidx
        rts

* = $1000 "SID"
.fill muzak.size, muzak.getData(i)

* = * "scroller code"
.import source "8x8.asm"

.align $100
* = * "FPP ROUTINES 2"
movefpplogo:
        ldy fppmovexidx:#00
        lda fppmove9thtab, y
        sta d10th
        lda tabx1, y
        sta fppposx + fppposstep*0
        lda tabx2, y
        sta fppposx + fppposstep*1
        lda tabx3, y
        sta fppposx + fppposstep*2
        lda tabx4, y
        sta fppposx + fppposstep*3
        lda tabx5, y
        sta fppposx + fppposstep*4
        lda tabx6, y
        sta fppposx + fppposstep*5

        //inc fppmovexidx
        lax fppmovexidx
        axs sp:#1
        stx fppmovexidx
        bne h
        ldx sp
        inx
        cpx #$04
        bne g
        ldx #$01
g:
        stx sp
h:
        rts

fppskip:
        clc
        ldy fppskipidx:#50
        ldx #00
l:
        lda rastersin, y
        lsr
        lsr
        lsr
        beq !+
        adc fppcopyaddr
        sta fppcopyaddr
!:
        stx fppcopyaddr:fppshaddowzp
        inc fppcopyaddr
        iny
        inx
        cpx #32
        bne l
        inc fppskipidx

        lda #fppshaddowzp
        sta fppcopyaddr
        jsr copyshadowfpp
        rts



scrollraster:
        jsr clearfpp
        lda rasteridx:#00
        and #63
        tay
        lax rastersin, y
        .for (var i = 0; i < rasterbar1.size(); i++)
        {
                lda #rasterbar1.get(i)
                sta fppshaddowzp + i, x
        }
        lda rasteridx2:#70
        and #63
        tay
        lax rastersin, y
        .for (var i = 0; i < rasterbar2.size(); i++)
        {
                lda #rasterbar2.get(i)
                sta fppshaddowzp + i, x
        }
// these zeros are replaced by a code commented below.
// would be better to move the whole function to a separate
// piece of memory and just generate there but zeros pack
// really well so it doesn't matter that much
scrollraster_speedcode:
        .for (var i = 0; i < 64; i++)
        {
            .byte $00, $00
            .byte $00, $00, $00
        }
        /*
        .for (var i = 0; i < 64; i++)
        {
                lda fppshaddowzp + i
                sta bgline + i*bgstep
        }
        */
        dec rasteridx
        inc rasteridx2
        rts

.align $100
* = * "tables"
rastersin:
.byte 25,22,20,17,15,13,10,8,7,5,3,2,1,0,0,0
.byte 0,0,0,1,2,3,4,6,7,9,11,14,16,18,21,23
.byte 26,28,31,33,35,38,40,42,43,45,46,47,48,49,49,49
.byte 49,49,49,48,47,46,44,42,41,39,36,34,32,29,27,25

.byte 0,0,0,0,1,1,2,3,4,5,6,7,8,10,11,12
.byte 13,14,15,16,17,17,18,18,18,19,19,18,18,18,17,17
.byte 16,15,14,14,13,12,11,10,10,9,8,8,8,8,7,8
.byte 8,8,9,9,10,11,12,13,14,16,17,18,20,21,22,23
.byte 25,26,27,28,28,29,30,30,30,30,30,30,30,29,28,28
.byte 27,26,25,24,23,22,21,20,19,19,18,17,17,16,16,16
.byte 16,16,16,17,17,18,19,19,20,21,22,23,24,25,26,27
.byte 28,28,29,30,30,30,30,30,30,30,29,28,28,27,26,25
.byte 23,22,21,20,18,17,16,14,13,12,11,10,9,9,8,8
.byte 8,7,8,8,8,8,9,10,10,11,12,13,14,14,15,16
.byte 17,17,18,18,18,19,19,18,18,18,17,17,16,15,14,13
.byte 12,11,10,8,7,6,5,4,3,2,1,1,0,0,0,0

fppgotab:
.byte 60,58,57,55,54,52,51,49,48,46,45,43,42,41,39,38
.byte 36,35,34,32,31,30,29,27,26,25,24,22,21,20,19,18
.byte 17,16,15,14,13,12,11,10,9,9,8,7,6,6,5,5
.byte 4,3,3,2,2,2,1,1,1,0,0,0,0,0,0,0
.byte 0,0,0,0,0,0,0,0,1,1,1,2,2,3,3,4
.byte 4,5,5,6,7,8,8,9,10,11,12,13,13,14,15,16
.byte 17,19,20,21,22,23,24,25,27,28,29,30,32,33,34,36
.byte 37,39,40,41,43,44,46,47,48,50,51,53,54,56,57,59
.byte 60,62,63,65,66,68,69,71,72,73,75,76,78,79,80,82
.byte 83,85,86,87,89,90,91,92,94,95,96,97,98,99,100,102
.byte 103,104,105,106,106,107,108,109,110,111,111,112,113,114,114,115
.byte 115,116,116,117,117,118,118,118,119,119,119,119,119,119,119,119
.byte 119,119,119,119,119,119,119,118,118,118,117,117,117,116,116,115
.byte 114,114,113,113,112,111,110,110,109,108,107,106,105,104,103,102
.byte 101,100,99,98,97,95,94,93,92,90,89,88,87,85,84,83
.byte 81,80,78,77,76,74,73,71,70,68,67,65,64,62,61,60

.var movex = List().add(
  0,0,0,0,0,1,1,2,3,4,5,6,7,8,10,11
, 13,15,16,18,20,22,25,27,29,32,34,37,40,42,45,48
, 51,54,57,61,64,67,71,74,78,81,85,89,93,96,100,104
, 108,112,116,120,125,129,133,137,141,146,150,154,158,163,167,171
, 176,180,184,188,193,197,201,206,210,214,218,222,227,231,235,239
, 243,247,251,254,258,262,266,269,273,276,280,283,287,290,293,296
, 299,302,305,308,311,314,316,319,321,323,326,328,330,332,333,335
, 337,338,340,341,343,344,345,346,347,347,348,348,349,349,349,349
, 349,349,349,349,348,348,347,347,346,345,344,343,341,340,338,337
, 335,333,332,330,328,326,323,321,319,316,314,311,308,305,302,299
, 296,293,290,287,283,280,276,273,269,266,262,258,254,251,247,243
, 239,235,231,227,222,218,214,210,206,201,197,193,188,184,180,176
, 171,167,163,158,154,150,146,141,137,133,129,125,120,116,112,108
, 104,100,96,93,89,85,81,78,74,71,67,64,61,57,54,51
, 48,45,42,40,37,34,32,29,27,25,22,20,18,16,15,13
, 11,10,8,7,6,5,4,3,2,1,1,0,0,0,0,0
)

orgtablo:
.fill 128, <(movex.get(i) - xsinezero)
orgtabhi:
.fill 128, >(movex.get(i) - xsinezero)

tab03:
.byte $0,$6,$b,$4,$c,$3,$3,$3
tab04:
.byte $0,$6,$b,$4,$4,$4,$4,$4
tab06:
.byte $0,$6,$6,$6,$6,$6,$6,$6
tab0a:
.byte $0,$9,$2,$4,$a,$a,$a,$a
tab0e:
.byte $0,$6,$b,$4,$c,$e,$e,$e
tab01:
.byte $0,$0,$0,$0,$0,$0,$0,$0

.var fpplogo = LoadPicture("desire.png", List().add($0, $8e3c97, $2e2c9b, $c46c71))
fppgfx:
.for (var i = 0; i < 31; i++)
{
    .for (var s = 0; s < 6; s++)
    {
        .byte fpplogo.getMulticolorByte(s*3 + 0, i)
        .byte fpplogo.getMulticolorByte(s*3 + 1, i)
        .byte fpplogo.getMulticolorByte(s*3 + 2, i)
    }
}


.macro copy_fpp_sprites()
{
        ldy #31
begin:
        ldx #(3*6)-1
!:
        lda src:fppgfx,x
        sta dst:$4000 + 64 + 9,x
        dex
        bpl !-
        clc
        lda src+0
        adc #3*6
        sta src+0
        bcc !+
        inc src+1
        clc
!:
        lda dst+0
        adc #64
        sta dst+0
        bcc !+
        inc dst+1
!:
        dey
        bne begin
}

// this is how the sprite data is in the memory after the copy_fpp_sprites routine.
// being packed + this little routine produces a lot smaller file size
// than them being expanded already. 
/*
* = $4000 "FPP SPRITES (32)"
.var fpplogo = LoadPicture("desire.png", List().add($0, $8e3c97, $2e2c9b, $c46c71))
.fill 64, $00
.for (var i = 0; i < 31; i++)
{
    .for (var w = 0; w < 3; w++)
    {
        .byte $00,$00,$00
    }
    .for (var s = 0; s < 6; s++)
    {
        .byte fpplogo.getMulticolorByte(s*3 + 0, i)
        .byte fpplogo.getMulticolorByte(s*3 + 1, i)
        .byte fpplogo.getMulticolorByte(s*3 + 2, i)
    }
    .for (var w = 0; w < 21-9; w++)
    {
        .byte $00,$00,$00
    }
    .byte $00
}
.fill $5100 - *, $00
* = * "after vic"
*/

// virtual means this is not included in the file.
// the compiler acts as if these tables are in the file though
// these buffors are filled in at the start of a program
* = $5100 "sinus tables" virtual
tabx1:
.fill $100,$00
tabx2:
.fill $100,$00
tabx3:
.fill $100,$00
tabx4:
.fill $100,$00
tabx5:
.fill $100,$00
tabx6:
.fill $100,$00
fppmove9thtab:
.fill $100,$00
rastersin_lsr:
.fill $100,$00