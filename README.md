# Desire 4K for Moonshine

Source code of a 4KB Commodore 64 intro called 4K for Moonshine by Desire. Download binary here https://csdb.dk/release/?id=178059

Credits:
- code : Golara
- graphics : VisionVortex
- music : No-XS

------------

To compile you need KickAssembler (http://www.theweb.dk/KickAssembler). Compiled binary will be around 7kb. To create self extracting archive under 4kb use ALZ64 (https://csdb.dk/release/?id=77754)

------------

To compile (produces main.prg file):
`kickasm main.asm`
To compress (produces alz.prg file):
`ALZ64.exe -1 $35 -s main.prg alz.prg`